<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

$this->title = Yii::t('app', 'Очередь');
$this->params['breadcrumbs'][] = $this->title;
$status = '';
?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?= $this->title ?></div>
		<div class="panel-body">
<div class="contact-index">
	<div class="fa-br"></div>
	<br>
	<?php
	$dataProvider = new ActiveDataProvider([
		'query' => $model,
		'pagination' => [
		 'pageSize' => 20,
		],
	]);
	echo GridView::widget([
		'dataProvider' => $dataProvider,
		'layout' => "{items}\n{pager}",
		'columns' => [
			// ['class' => 'yii\grid\SerialColumn'],
			'id',
			'code',
			[
				'label' => 'Статус',
				'attribute' => 'status',
				'format' => 'raw',
				'value' => function($dataProvider){
					return ($dataProvider->status == 0) ? 'В очереди' : 'Прошел';
				},
			],
			[
				'label' => 'Действие',
				'format' => 'raw',
				'value' => function($dataProvider){
					if($dataProvider->status == 0)
						return Html::a("Прошел", ['site/update', 'id' => $dataProvider->id]);
					else
						return "Уже Прошел";
				},
			],
		],
	]);
	?>

</div>

		</div>
	</div>
</div>
