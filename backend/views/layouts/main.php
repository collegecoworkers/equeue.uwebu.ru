<?php

use backend\assets\AppAsset;
use common\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);

$modules_id = Yii::$app->controller->module->id;
$controller_id = Yii::$app->controller->id;
$action_id = Yii::$app->controller->action->id;
$entity_id = isset($_GET['id']) ? (int)$_GET['id'] : null;

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <script type="text/javascript">
            var base_url = '<?= Url::base() ?>';
            var modules_name = '<?= $modules_id ?>';
            var controller_name = '<?= $controller_id ?>';
            var action_name = '<?= $action_id ?>';
            var entity_id = '<?= $entity_id ?>';
        </script>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="header">
        <div class="header-split" style="float: left; margin-top: 18px;">
        <div class="raspina-logo"><?= Html::a('Админ панель', ['/']) ?></div>
        </div>

    <?php 
        $header_split_s = [
            [
                'url'  => '/site/logout', 
                'icon' => 'sign-out',
                'label' => 'Выход',
            ],
            [
                'url'  => '/site/all', 
                'icon' => 'user',
                'label' => 'Весь список',
            ],
            [
                'url'  => '/', 
                'icon' => 'home',
                'label' => 'Главная',
            ],
        ];    
     ?>

    <?php if (!Yii::$app->user->isGuest): ?>
            <?php foreach ($header_split_s as $header_split): ?>
                <div class="header-split" style="margin-top: 8px;">
                    <a href="<?= Url::base() . $header_split['url'] ?>">
                        <div><span class="fa fa-<?= $header_split['icon'] ?>"></span></div>
                        <div class="menu-title"><?= $header_split['label'] ?></div>
                    </a>
                </div>
            <?php endforeach ?>
    <?php endif ?>

    </div>


    <div class="pull-left main-content">
        <div class="col-md-12">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
        </div>
        <?= $content ?>
    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>