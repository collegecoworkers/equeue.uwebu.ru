<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
use dosamigos\datetimepicker\DateTimePicker;

/* @var $this yii\web\View */

$this->title = 'Регистратура';
?>

<div style="height: calc(100vh - 100px);display: flex;align-items: center;justify-content: center;">
    <?php $form = ActiveForm::begin() ?>
    <?= Html::submitButton(Yii::t('app', 'Встать в очередь'), ['class' =>  'btn btn-success', 'style' => 'width: 200px; height: 50px;display: block;margin: 0 auto;']) ?>
    <?php ActiveForm::end() ?>
</div>
