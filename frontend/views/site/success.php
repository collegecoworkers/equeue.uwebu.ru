<?php

use yii\helpers\Html;

$this->title = 'Успех';

?>
<div class="site-error">

    <div class="alert alert-success">
        <h1>
        		Ваш код
            <?= $code ?>
        </h1>
    </div>

</div>
