(function(old) {
  $.fn.attr = function() {
    if(arguments.length === 0) {
      if(this.length === 0) {
        return null;
      }

      var obj = {};
      $.each(this[0].attributes, function() {
        if(this.specified) {
          obj[this.name] = this.value;
        }
      });
      return obj;
    }

    return old.apply(this, arguments);
  };
})($.fn.attr);

$(document).ready(function(){
	var options = {
		autoclose: true,
		showSecond: true,
		// timeFormat: 'hh:mm:ss',
		format: 'yyyy-mm-dd  hh:ii:00',
	};

	var attrs = $('.datetimepicker').attr();
	for(var key in attrs){
		if(key.match('^u-.*')){
			options[key.replace(/^u-/, '')] = attrs[key];
		}
	}
	console.log(options);

	$('.datetimepicker').datetimepicker(options);
	var d = new Date();
	var curr_date = d.getDate()+1;
	var curr_month = d.getMonth() + 1;
	var curr_year = d.getFullYear();

	$('.datetimepicker').datetimepicker('setStartDate', curr_year + "-" + curr_month + "-" + curr_date);
});
