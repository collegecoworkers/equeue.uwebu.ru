<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Equeue;

class SiteController extends Controller
{

	public function behaviors()
	{
		return [];
	}

	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex()
	{
		$model = new Equeue();

		if (Yii::$app->request->post() && $model->validate()) {
			$model->save();

			$model->code = strtoupper($this->generateRandomString(3) . Yii::$app->db->getLastInsertID());
			$model->status = 0;

			$model->save();

			return $this->render('success', [
				'code' => $model->code
			]);
		}

		return $this->render('index', [
			'model' => $model,
		]);
	}

	private function generateRandomString($length = 10) {

		$characters = '0123456789abcdefABCDEF';
		$charactersLength = strlen($characters);
		$randomString = '';

		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

		return $randomString;
	}
}
