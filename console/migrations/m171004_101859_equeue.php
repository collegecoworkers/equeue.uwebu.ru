<?php

use yii\db\Migration;

class m171004_101859_equeue extends Migration
{
    public function safeUp()
    {
        $this->createTable('equeue', [
            'id' => $this->primaryKey(),
            'code' => $this->string(20),
            'status' => $this->integer()->defaultValue(0),
        ]);
    }

    public function safeDown()
    {
        return false;
    }
}
